from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

app = Flask(__name__)
POSTGRES = {
    'user': 'root',
    'pw': '1234',
    'db': 'Secim-Takip',
    'host': 'localhost',
    'port': '5432',
}
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)


class Kullanicilar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ad_soyad = db.Column(db.String, nullable=False)
    kullanici_adi = db.Column(db.String, nullable=False, unique=True)
    sifre = db.Column(db.String, nullable=False)
    yetki = db.Column(db.Integer, nullable=False)
    is_active = db.Column(db.Boolean, nullable=False)
    olusturulma_tarihi = db.Column(db.DateTime, nullable=False)
    son_gorulme = db.Column(db.DateTime, nullable=False)
    mesajlarf = db.relationship('Mesajlar', backref='kullanicilar', lazy=True)
    loglarf = db.relationship('Loglar', backref='kullanicilar', lazy=True)


class Mahalleler(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mahalle_adi = db.Column(db.String, nullable=False,unique=True)
    mahallef = db.relationship('Okullar', backref='mahalleler', lazy=True)


class Okullar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    okul_adi = db.Column(db.String, nullable=False)
    mahalle_id = db.Column(db.Integer, db.ForeignKey('mahalleler.id'),nullable=False)
    sandikf = db.relationship('Sandiklar', backref='okullar', lazy=True)


class Sandiklar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    okul_id = db.Column(db.Integer, db.ForeignKey('okullar.id'),nullable=False)
    sandik_no = db.Column(db.String, nullable=False,unique=True)
    musahitlerf = db.relationship('Musahitler', backref='sandiklar', lazy=True)
    oylarf = db.relationship('Oylar', backref='sandiklar', lazy=True)


class Musahitler(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sandik_id = db.Column(db.Integer, db.ForeignKey('sandiklar.id'),nullable=False)
    musahit_adi = db.Column(db.String, nullable=False)
    musahit_tel = db.Column(db.String, nullable=True)
    durum = db.Column(db.Integer, nullable=False)
    aciklama = db.Column(db.String, nullable=True)


class Partiler(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    parti_adi = db.Column(db.String, nullable=False,unique=True)
    partif = db.relationship('Oylar', backref='partiler', lazy=True)


class Oylar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sandik_id = db.Column(db.Integer, db.ForeignKey('sandiklar.id'), nullable=False)
    parti_id = db.Column(db.Integer, db.ForeignKey('partiler.id'), nullable=False)
    oylar = db.Column(db.Integer, nullable=False)
    son_guncelleme = db.Column(db.DateTime, nullable=False)


class Loglar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    durum = db.Column(db.String, nullable=False)
    veri = db.Column(db.JSON, nullable=True)
    ip_adresi = db.Column(db.String, nullable=False)
    kullanici_id = db.Column(db.Integer, db.ForeignKey('kullanicilar.id'), nullable=False)
    olusturulma_tarihi = db.Column(db.DateTime, nullable=False)


class Mesajlar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mesaj = db.Column(db.String, nullable=False)
    ip_adresi = db.Column(db.String, nullable=False)
    kullanici_id = db.Column(db.Integer, db.ForeignKey('kullanicilar.id'), nullable=False)
    olusturulma_tarihi = db.Column(db.DateTime, nullable=False)


if __name__ == '__main__':
    manager.run()