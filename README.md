# Seçim Takip

**İlk defa migration oluşturma:**

`python manage.py db init`


**Migration oluşturma:**

`python manage.py db migrate`


**Veritabanındaki tabloları güncelleme:**

`python manage.py db upgrade`
