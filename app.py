from datetime import datetime
from flask import Flask, request, jsonify, render_template, redirect, url_for, json
from flask_login import LoginManager, login_required, current_user, login_user, logout_user
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc, func
import plotly
import plotly.plotly as py
import plotly.graph_objs as go

app = Flask(__name__)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "giris"
login_manager.session_protection = "yqoRzuM2fKtOCEO4P9reu6nKXa9vUYB4"

POSTGRES = {
    'user': 'root',
    'pw': '1234',
    'db': 'Secim-Takip',
    'host': 'localhost',
    'port': '5432',
}
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = "yqoRzuM2fKtOCEO4P9reu6nKXa9vUYB4"

db = SQLAlchemy(app)


class Kullanicilar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ad_soyad = db.Column(db.String, nullable=False)
    kullanici_adi = db.Column(db.String, nullable=False, unique=True)
    sifre = db.Column(db.String, nullable=False)
    yetki = db.Column(db.Integer, nullable=False)
    is_active = db.Column(db.Boolean, nullable=False)
    olusturulma_tarihi = db.Column(db.DateTime, nullable=False)
    son_gorulme = db.Column(db.DateTime, nullable=False)
    mesajlarf = db.relationship('Mesajlar', backref='kullanicilar', lazy=True)
    loglarf = db.relationship('Loglar', backref='kullanicilar', lazy=True)

    def get_id(self):
        return self.id

    def is_authenticated(self):
        return True

    def is_activex(self):  # line 37
        return self.is_active

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username


class Mahalleler(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mahalle_adi = db.Column(db.String, nullable=False,unique=True)
    mahallef = db.relationship('Okullar', backref='mahalleler', lazy=True)


class Okullar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    okul_adi = db.Column(db.String, nullable=False)
    mahalle_id = db.Column(db.Integer, db.ForeignKey('mahalleler.id'),nullable=False)
    sandikf = db.relationship('Sandiklar', backref='okullar', lazy=True)


class Sandiklar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    okul_id = db.Column(db.Integer, db.ForeignKey('okullar.id'),nullable=False)
    sandik_no = db.Column(db.String, nullable=False,unique=True)
    musahitlerf = db.relationship('Musahitler', backref='sandiklar', lazy=True)
    oylarf = db.relationship('Oylar', backref='sandiklar', lazy=True)


class Musahitler(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sandik_id = db.Column(db.Integer, db.ForeignKey('sandiklar.id'),nullable=False)
    musahit_adi = db.Column(db.String, nullable=False)
    musahit_tel = db.Column(db.String, nullable=True)
    durum = db.Column(db.Integer, nullable=False)
    aciklama = db.Column(db.String, nullable=True)


class Partiler(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    parti_adi = db.Column(db.String, nullable=False,unique=True)
    partif = db.relationship('Oylar', backref='partiler', lazy=True)


class Oylar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sandik_id = db.Column(db.Integer, db.ForeignKey('sandiklar.id'), nullable=False)
    parti_id = db.Column(db.Integer, db.ForeignKey('partiler.id'), nullable=False)
    oylar = db.Column(db.Integer, nullable=False)
    son_guncelleme = db.Column(db.DateTime, nullable=False)


class Loglar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    durum = db.Column(db.String, nullable=False)
    veri = db.Column(db.JSON, nullable=True)
    ip_adresi = db.Column(db.String, nullable=False)
    kullanici_id = db.Column(db.Integer, db.ForeignKey('kullanicilar.id'), nullable=False)
    olusturulma_tarihi = db.Column(db.DateTime, nullable=False)


class Mesajlar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mesaj = db.Column(db.String, nullable=False)
    ip_adresi = db.Column(db.String, nullable=False)
    kullanici_id = db.Column(db.Integer, db.ForeignKey('kullanicilar.id'), nullable=False)
    olusturulma_tarihi = db.Column(db.DateTime, nullable=False)


@login_manager.user_loader
def get_user(ident):
    return Kullanicilar.query.get(int(ident))


def log_ekle(durum,veri):
    log = Loglar()
    log.kullanici_id = current_user.id
    log.ip_adresi = request.remote_addr
    log.durum = durum
    log.veri = veri
    log.olusturulma_tarihi = datetime.utcnow()
    db.session.add(log)
    db.session.commit()
    return True


@app.route('/cikis')
@login_required
def logout():
    log_ekle("Çıkış", None)
    logout_user()
    return redirect(url_for('giris'))


@app.route('/')
@login_required
def pano():
    dsp = db.session.query(func.sum(Oylar.oylar)).filter_by(parti_id=1).group_by(Oylar.parti_id).first()
    mhp = db.session.query(func.sum(Oylar.oylar)).filter_by(parti_id=3).group_by(Oylar.parti_id).first()
    chp = db.session.query(func.sum(Oylar.oylar)).filter_by(parti_id=2).group_by(Oylar.parti_id).first()
    diger = db.session.query(func.sum(Oylar.oylar)).filter_by(parti_id=4).group_by(Oylar.parti_id).first()
    labels = ['DSP', 'MHP', 'CHP', 'DİGER']
    values = [dsp[0], mhp[0], chp[0], diger[0]]
    colors = ['#17C2EF', '#CC0000', '#ED1C24', '#E9ECEF']

    trace = go.Pie(labels=labels, values=values, marker=dict(colors=colors))
    data = [trace]
    graphJSON = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    tarih_getir = db.session.query(Oylar.son_guncelleme).order_by(desc(Oylar.son_guncelleme)).limit(1).first()
    tarih_getir = tarih_getir.son_guncelleme
    return render_template('anasayfa.html',graphJSON=graphJSON,tarih_getir=tarih_getir)


@app.route('/giris', methods=['GET'])
def giris():
    return render_template('login.html')


@app.route('/girispost', methods=['POST'])
def giris_post():
    ka = request.form['ka']
    sf = request.form['sf']
    kullanici = Kullanicilar.query.filter_by(kullanici_adi=ka).filter_by(sifre=sf).first()
    if kullanici is None:
        return jsonify(sonuc="Kullanıcı Adı veya Şifre doğru değil", durum=False)
    elif kullanici.yetki is 2:
        return jsonify(result="Giriş başarısız", durum=False)
    else:
        login_user(kullanici)
        log_ekle("Giriş", None)
        return jsonify(result="Hoşgeldin,"+kullanici.ad_soyad, durum=True)


@app.route('/kullanicilar', methods=['GET'])
@login_required
def kullanicilar():
    if current_user.yetki is 0:
        page = request.args.get('page', 1, type=int)
        kullanicilar = db.session.query(Kullanicilar).order_by(desc(Kullanicilar.id)).paginate(page, 15, False)
        next_url = url_for('kullanicilar', page=kullanicilar.next_num) \
            if kullanicilar.has_next else None
        prev_url = url_for('kullanicilar', page=kullanicilar.prev_num) \
            if kullanicilar.has_prev else None
        return render_template('kullanicilar.html', kullanicilar=kullanicilar, next_url=next_url, prev_url=prev_url)
    else:
        return render_template('404.html')


@app.route('/YeniKullanici', methods=['POST'])
@login_required
def yeni_kullanici():
    ad_soyad = request.form['ad_soyad']
    kullanici_adi = request.form['kullanici_adi']
    sifre = request.form['sifre']
    yetki = request.form['yetki']
    if ad_soyad.strip() is "":
        return jsonify(result="Ad Soyad boş bırakılamaz",status=False)
    elif kullanici_adi.strip() is "":
        return jsonify(result="Kullanıcı Adı boş bırakılamaz",status=False)
    elif sifre.strip() is "":
        return jsonify(result="Şifre boş bırakılamaz",status=False)
    elif yetki.strip() is "":
        return jsonify(result="Yetki boş bırakılamaz",status=False)
    elif current_user.yetki is not 0:
        return jsonify(result="Geçersiz yetki",status=False)
    else:
        kullanici = Kullanicilar()
        kullanici.ad_soyad = ad_soyad
        kullanici.kullanici_adi = kullanici_adi
        kullanici.sifre = sifre
        kullanici.yetki = yetki
        kullanici.is_active = True
        kullanici.olusturulma_tarihi = datetime.utcnow()
        kullanici.son_gorulme = datetime.utcnow()
        db.session.add(kullanici)
        db.session.commit()
        log_ekle("Kullanıcı Oluşturuldu", None)
        return jsonify(result="Kullanıcı Oluşturuldu",status=True)


@app.route('/mahalleler', methods=['GET'])
@login_required
def mahalleler():
    if current_user.yetki is 0:
        page = request.args.get('page', 1, type=int)
        mahalleler = db.session.query(Mahalleler).order_by(desc(Mahalleler.id)).paginate(page, 15, False)
        next_url = url_for('mahalleler', page=mahalleler.next_num) \
            if mahalleler.has_next else None
        prev_url = url_for('mahalleler', page=mahalleler.prev_num) \
            if mahalleler.has_prev else None
        return render_template('mahalleler.html', mahalleler=mahalleler, next_url=next_url, prev_url=prev_url)
    else:
        return render_template('404.html')


@app.route('/YeniMahalle', methods=['POST'])
@login_required
def yeni_mahalle():
    mahalle_adi = request.form['mahalle_adi']
    if mahalle_adi.strip() is "":
        return jsonify(result="Mahalle Adı boş bırakılamaz",status=False)
    elif current_user.yetki is not 0:
        return jsonify(result="Geçersiz yetki",status=False)
    else:
        mahalle = Mahalleler()
        mahalle.mahalle_adi = mahalle_adi
        db.session.add(mahalle)
        db.session.commit()
        log_ekle("Mahalle Eklendi", None)
        return jsonify(result="Mahalle Eklendi",status=True)


@app.route('/okullar', methods=['GET'])
@login_required
def okullar():
    if current_user.yetki is 0:
        page = request.args.get('page', 1, type=int)
        okullar = db.session.query(Okullar.okul_adi,Okullar.id,Mahalleler.mahalle_adi).join(Mahalleler,Okullar.mahalle_id == Mahalleler.id).order_by(desc(Okullar.id)).paginate(page, 15, False)
        next_url = url_for('okullar', page=okullar.next_num) \
            if okullar.has_next else None
        prev_url = url_for('okullar', page=okullar.prev_num) \
            if okullar.has_prev else None
        return render_template('okullar.html', okullar=okullar, next_url=next_url, prev_url=prev_url)
    else:
        return render_template('404.html')


@app.route('/YeniOkul', methods=['GET'])
def yeni_okul():
    if current_user.yetki is 0:
        mahalleler = db.session.query(Mahalleler).order_by(Mahalleler.mahalle_adi).all()
        return render_template('YeniOkul.html', mahalleler=mahalleler)
    else:
        return render_template('404.html')


@app.route('/YeniOkulPost', methods=['POST'])
@login_required
def yeni_okul_post():
    okul_adi = request.form['okul_adi']
    mahalle_id = request.form['mahalle_id']
    if okul_adi.strip() is "":
        return jsonify(result="Okul Adı boş bırakılamaz",status=False)
    elif mahalle_id.strip() is "":
        return jsonify(result="Mahalle Adı boş bırakılamaz", status=False)
    elif current_user.yetki is not 0:
        return jsonify(result="Geçersiz yetki",status=False)
    else:
        okul = Okullar()
        okul.okul_adi = okul_adi
        okul.mahalle_id = mahalle_id
        db.session.add(okul)
        db.session.commit()
        log_ekle("Okul Eklendi", None)
        return jsonify(result="Okul Eklendi",status=True)


@app.route('/sandiklar', methods=['GET'])
@login_required
def sandiklar():
    if current_user.yetki is 0:
        page = request.args.get('page', 1, type=int)
        sandiklar = db.session.query(Sandiklar.sandik_no,Sandiklar.id,Okullar.okul_adi).join(Okullar,Sandiklar.okul_id == Okullar.id).order_by(desc(Sandiklar.sandik_no)).paginate(page, 15, False)
        next_url = url_for('sandiklar', page=sandiklar.next_num) \
            if sandiklar.has_next else None
        prev_url = url_for('sandiklar', page=sandiklar.prev_num) \
            if sandiklar.has_prev else None
        return render_template('sandiklar.html', sandiklar=sandiklar, next_url=next_url, prev_url=prev_url)
    else:
        return render_template('404.html')


@app.route('/YeniSandik', methods=['GET'])
@login_required
def yeni_sandik():
    if current_user.yetki is 0:
        okullar = db.session.query(Okullar).order_by(Okullar.okul_adi).all()
        return render_template('YeniSandik.html', okullar=okullar)
    else:
        return render_template('404.html')


@app.route('/YeniSandikPost', methods=['POST'])
@login_required
def yeni_sandik_post():
    sandik_adi = request.form['sandik_adi']
    okul_id = request.form['okul_id']
    if sandik_adi.strip() is "":
        return jsonify(result="Sandık Adı boş bırakılamaz",status=False)
    elif okul_id.strip() is "":
        return jsonify(result="Okul Adı boş bırakılamaz", status=False)
    elif current_user.yetki is not 0:
        return jsonify(result="Geçersiz yetki",status=False)
    else:
        sandik = Sandiklar()
        sandik.sandik_no = sandik_adi
        sandik.okul_id = okul_id
        db.session.add(sandik)
        db.session.commit()
        log_ekle("Sandık Eklendi", None)
        return jsonify(result="Sandık Eklendi",status=True)


@app.route('/musahitler', methods=['GET'])
@login_required
def musahitler():
    if current_user.yetki is 0:
        page = request.args.get('page', 1, type=int)
        musahitler = db.session.query(Musahitler.id,Musahitler.musahit_adi,Musahitler.musahit_tel,Musahitler.durum,Musahitler.aciklama,Sandiklar.sandik_no).join(Sandiklar,Musahitler.sandik_id == Sandiklar.id).order_by(desc(Musahitler.musahit_adi)).paginate(page, 15, False)
        next_url = url_for('musahitler', page=musahitler.next_num) \
            if musahitler.has_next else None
        prev_url = url_for('musahitler', page=musahitler.prev_num) \
            if musahitler.has_prev else None
        return render_template('musahitler.html', musahitler=musahitler, next_url=next_url, prev_url=prev_url)
    else:
        return render_template('404.html')


@app.route('/YeniMusahit', methods=['GET'])
@login_required
def yeni_musahit():
    if current_user.yetki is 0:
        sandiklar = db.session.query(Sandiklar).order_by(Sandiklar.sandik_no).all()
        return render_template('YeniMusahit.html', sandiklar=sandiklar)
    else:
        return render_template('404.html')


@app.route('/YeniMusahitPost', methods=['POST'])
@login_required
def yeni_musahit_post():
    musahit_adi = request.form['musahit_adi']
    musahit_tel = request.form['musahit_tel']
    aciklama = request.form['aciklama']
    durum = request.form['durum']
    sandik_id = request.form['sandik_id']
    if musahit_adi.strip() is "":
        return jsonify(result="Müşahit Adı boş bırakılamaz",status=False)
    elif sandik_id.strip() is "":
        return jsonify(result="Sandık Adı boş bırakılamaz", status=False)
    elif current_user.yetki is not 0:
        return jsonify(result="Geçersiz yetki",status=False)
    else:
        musahit = Musahitler()
        musahit.musahit_adi = musahit_adi
        musahit.musahit_tel = musahit_tel
        musahit.durum = durum
        musahit.sandik_id = sandik_id
        musahit.aciklama = aciklama
        db.session.add(musahit)
        db.session.commit()
        log_ekle("Müşahit Eklendi", None)
        return jsonify(result="Müşahit Eklendi",status=True)


@app.route('/partiler', methods=['GET'])
@login_required
def partiler():
    if current_user.yetki is 0:
        page = request.args.get('page', 1, type=int)
        partiler = db.session.query(Partiler).order_by(Partiler.parti_adi).paginate(page, 15, False)
        next_url = url_for('partiler', page=partiler.next_num) \
            if partiler.has_next else None
        prev_url = url_for('partiler', page=partiler.prev_num) \
            if partiler.has_prev else None
        return render_template('partiler.html', partiler=partiler, next_url=next_url, prev_url=prev_url)
    else:
        return render_template('404.html')


@app.route('/YeniParti', methods=['POST'])
@login_required
def yeni_parti():
    parti_adi = request.form['parti_adi']
    if parti_adi.strip() is "":
        return jsonify(result="Parti Adı boş bırakılamaz",status=False)
    elif current_user.yetki is not 0:
        return jsonify(result="Geçersiz yetki",status=False)
    else:
        parti = Partiler()
        parti.parti_adi = parti_adi
        db.session.add(parti)
        db.session.commit()
        log_ekle("Parti Eklendi", None)
        return jsonify(result="Parti Eklendi",status=True)


@app.route('/OyEkle', methods=['GET'])
@login_required
def oy_ekle():
    if current_user.yetki is 0:
        okullar = db.session.query(Okullar).order_by(Okullar.okul_adi).all()
        partiler = db.session.query(Partiler).order_by(Partiler.parti_adi).all()
        sandiklar = db.session.query(Sandiklar.id,Sandiklar.sandik_no,Okullar.okul_adi).order_by(Sandiklar.sandik_no).join(Okullar,Sandiklar.okul_id == Okullar.id).all()
        return render_template('oyekle.html', sandiklar=sandiklar, partiler=partiler, okullar=okullar)
    else:
        return render_template('404.html')


@app.route('/SandikGetir', methods=['POST'])
@login_required
def sandik_getir():
    if current_user.yetki is 0:
        id = request.form['id']
        sandiklar = db.session.query(Sandiklar.id, Sandiklar.sandik_no).filter_by(okul_id=id).order_by(
            Sandiklar.sandik_no).all()
        return render_template('sandikgetir.html', sandiklar=sandiklar)
    else:
        return render_template('404.html')


@app.route('/OyEklePost', methods=['POST'])
@login_required
def oy_ekle_post():
    sandik_id = request.form['sandiklar']
    dsp = request.form['dsp']
    chp = request.form['chp']
    mhp = request.form['mhp']
    diger = request.form['diger']
    if str(sandik_id).strip() is "":
        return jsonify(result="Sandık No boş bırakılamaz",status=False)
    elif str(dsp).strip() is "":
        return jsonify(result="Oy Sayısı boş bırakılamaz", status=False)
    elif int(dsp) < 0:
        return jsonify(result="Oy sayısı sıfırdan küçük olamaz", status=False)
    elif str(chp).strip() is "":
        return jsonify(result="Oy Sayısı boş bırakılamaz", status=False)
    elif int(chp) < 0:
        return jsonify(result="Oy sayısı sıfırdan küçük olamaz", status=False)
    elif str(mhp).strip() is "":
        return jsonify(result="Oy Sayısı boş bırakılamaz", status=False)
    elif int(mhp) < 0:
        return jsonify(result="Oy sayısı sıfırdan küçük olamaz", status=False)
    elif str(diger).strip() is "":
        return jsonify(result="Oy Sayısı boş bırakılamaz", status=False)
    elif int(diger) < 0:
        return jsonify(result="Oy sayısı sıfırdan küçük olamaz", status=False)
    elif current_user.yetki is not 0:
        return jsonify(result="Geçersiz yetki",status=False)
    else:
        dsp_kontrol = db.session.query(Oylar).filter_by(parti_id=1).filter_by(sandik_id=sandik_id).first()
        if dsp_kontrol is None:
            oy = Oylar()
            oy.sandik_id = sandik_id
            oy.parti_id = 1
            oy.oylar = dsp
            oy.son_guncelleme = datetime.utcnow()
            db.session.add(oy)
            db.session.commit()
            log_ekle("Oy Eklendi", json.dumps({"oy_sayisi": dsp, "parti_adi": "dsp", "sandik_id": sandik_id}))
        else:
            oy = dsp_kontrol
            oy.oylar = dsp
            oy.son_guncelleme = datetime.utcnow()
            db.session.add(oy)
            db.session.commit()
            log_ekle("Oy Eklendi", json.dumps({"oy_sayisi": dsp, "parti_adi": "dsp", "sandik_id":sandik_id}))

        chp_kontrol = db.session.query(Oylar).filter_by(parti_id=2).filter_by(sandik_id=sandik_id).first()
        if chp_kontrol is None:
            oy = Oylar()
            oy.sandik_id = sandik_id
            oy.parti_id = 2
            oy.oylar = chp
            oy.son_guncelleme = datetime.utcnow()
            db.session.add(oy)
            db.session.commit()
            log_ekle("Oy Eklendi", json.dumps({"oy_sayisi": chp, "parti_adi": "chp", "sandik_id": sandik_id}))
        else:
            oy = chp_kontrol
            oy.oylar = chp
            oy.son_guncelleme = datetime.utcnow()
            db.session.add(oy)
            db.session.commit()
            log_ekle("Oy Eklendi", json.dumps({"oy_sayisi": chp, "parti_adi": "chp", "sandik_id": sandik_id}))

        mhp_kontrol = db.session.query(Oylar).filter_by(parti_id=3).filter_by(sandik_id=sandik_id).first()
        if mhp_kontrol is None:
            oy = Oylar()
            oy.sandik_id = sandik_id
            oy.parti_id = 3
            oy.oylar = mhp
            oy.son_guncelleme = datetime.utcnow()
            db.session.add(oy)
            db.session.commit()
            log_ekle("Oy Eklendi", json.dumps({"oy_sayisi": mhp, "parti_adi": "mhp", "sandik_id": sandik_id}))
        else:
            oy = mhp_kontrol
            oy.oylar = mhp
            oy.son_guncelleme = datetime.utcnow()
            db.session.add(oy)
            db.session.commit()
            log_ekle("Oy Eklendi", json.dumps({"oy_sayisi": mhp, "parti_adi": "mhp", "sandik_id": sandik_id}))

        diger_kontrol = db.session.query(Oylar).filter_by(parti_id=4).filter_by(sandik_id=sandik_id).first()
        if diger_kontrol is None:
            oy = Oylar()
            oy.sandik_id = sandik_id
            oy.parti_id = 4
            oy.oylar = diger
            oy.son_guncelleme = datetime.utcnow()
            db.session.add(oy)
            db.session.commit()
            log_ekle("Oy Eklendi", json.dumps({"oy_sayisi": diger, "parti_adi": "diger", "sandik_id": sandik_id}))
        else:
            oy = diger_kontrol
            oy.oylar = diger
            oy.son_guncelleme = datetime.utcnow()
            db.session.add(oy)
            db.session.commit()
            log_ekle("Oy Eklendi", json.dumps({"oy_sayisi": diger, "parti_adi": "diger", "sandik_id": sandik_id}))
        return jsonify(result="Oy Eklendi",status=True)


@app.route('/OySayisiGetir', methods=['POST'])
@login_required
def oy_sayisi_getir():
    if current_user.yetki is 0:
        sandik_id = request.form['sandiklar']
        dsp = db.session.query(Oylar.oylar).filter_by(parti_id=1).filter_by(sandik_id=sandik_id).first()
        chp = db.session.query(Oylar.oylar).filter_by(parti_id=2).filter_by(sandik_id=sandik_id).first()
        mhp = db.session.query(Oylar.oylar).filter_by(parti_id=3).filter_by(sandik_id=sandik_id).first()
        diger = db.session.query(Oylar.oylar).filter_by(parti_id=4).filter_by(sandik_id=sandik_id).first()

        if dsp is None:
            dsp = 0
        if chp is None:
            chp = 0
        if mhp is None:
            mhp = 0
        if diger is None:
            diger = 0
        else:
            dsp = dsp.oylar
            chp = chp.oylar
            mhp = mhp.oylar
            diger = diger.oylar
        return jsonify(dsp=dsp,chp=chp,mhp=mhp,diger=diger)
    else:
        dsp = -1
        chp = -1
        mhp = -1
        diger = -1
        return jsonify(dsp=dsp,chp=chp,mhp=mhp,diger=diger)


@app.route('/loglar', methods=['GET'])
@login_required
def loglar():
    if current_user.yetki is 0:
        page = request.args.get('page', 1, type=int)
        loglar = db.session.query(Loglar.id,Loglar.durum,Loglar.veri,Loglar.ip_adresi,Loglar.olusturulma_tarihi,Kullanicilar.ad_soyad,Kullanicilar.kullanici_adi).join(Kullanicilar,Loglar.kullanici_id == Kullanicilar.id).order_by(desc(Loglar.id)).paginate(page, 15, False)
        next_url = url_for('loglar', page=loglar.next_num) \
            if loglar.has_next else None
        prev_url = url_for('loglar', page=loglar.prev_num) \
            if loglar.has_prev else None
        return render_template('loglar.html', loglar=loglar, next_url=next_url, prev_url=prev_url)
    else:
        return render_template('404.html')


@app.route('/Yenileme', methods=['POST'])
@login_required
def yenileme():
    tarih_getir = db.session.query(Oylar.son_guncelleme).order_by(desc(Oylar.son_guncelleme)).limit(1).first()
    if tarih_getir is None:
        tarih = 0
    else:
        tarih = tarih_getir.son_guncelleme
    return jsonify(result=str(tarih))


if __name__ == '__main__':
    app.run()
